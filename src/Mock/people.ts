import chinese1 from '../assets/images/people/chinese-1.jpg';
import chinese2 from '../assets/images/people/chinese-2.jpg';
import japanese1 from '../assets/images/people/japanese-1.jpg';
import japanese2 from '../assets/images/people/japanese-2.jpg';
import japanese3 from '../assets/images/people/japanese-3.jpg';
import southKorean1 from '../assets/images/people/south-korean-1.jpg';
import southKorean2 from '../assets/images/people/south-korean-2.jpg';
import southKorean3 from '../assets/images/people/south-korean-3.jpg';
import thai1 from '../assets/images/people/thai-1.jpg';
import thai2 from '../assets/images/people/thai-2.jpg';

export { chinese1, chinese2, japanese1, japanese2, japanese3, southKorean1, southKorean2, southKorean3, thai1, thai2 };