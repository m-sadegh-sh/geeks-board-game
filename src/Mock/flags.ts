import china from '../assets/images/flags/china.svg';
import japan from '../assets/images/flags/japan.svg';
import southKorea from '../assets/images/flags/south-korea.svg';
import thailand from '../assets/images/flags/thailand.svg';

export { china, japan, southKorea, thailand };