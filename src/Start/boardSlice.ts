import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { china, japan, southKorea, thailand } from '../Mock/flags';
import { chinese1, chinese2, japanese1, japanese2, japanese3, southKorean1, southKorean2, southKorean3, thai1, thai2 } from '../Mock/people';

import { BoardState } from './types';
import shuffle from 'lodash.shuffle';

const initialState: BoardState = {
  shouldDisplayCounter: true,
  statistics: {
    achievedPoints: 0,
    lastMatchSucceeded: null
  },
  countryItems: [
    { isoCode: 'chn', icon: china, name: 'China' },
    { isoCode: 'jpn', icon: japan, name: 'Japan' },
    { isoCode: 'kor', icon: southKorea, name: 'South Korea' },
    { isoCode: 'tha', icon: thailand, name: 'Thailand' }
  ],
  personItems: [
    { id: 1, countryIsoCode: 'chn', picture: chinese1, isPortrait: true },
    { id: 2, countryIsoCode: 'chn', picture: chinese2, isPortrait: false },
    { id: 3, countryIsoCode: 'jpn', picture: japanese1, isPortrait: false },
    { id: 4, countryIsoCode: 'jpn', picture: japanese2, isPortrait: false },
    { id: 5, countryIsoCode: 'jpn', picture: japanese3, isPortrait: false },
    { id: 6, countryIsoCode: 'kor', picture: southKorean1, isPortrait: false },
    { id: 7, countryIsoCode: 'kor', picture: southKorean2, isPortrait: true },
    { id: 8, countryIsoCode: 'kor', picture: southKorean3, isPortrait: false },
    { id: 9, countryIsoCode: 'tha', picture: thai1, isPortrait: false },
    { id: 10, countryIsoCode: 'tha', picture: thai2, isPortrait: false }
  ],
  passedPersonIds: [],
  showingPersonId: null,
  isFinished: false
};

export const boardSlice = createSlice({
  name: 'board',
  initialState: initialState,
  reducers: {
    generateBoard: (state: BoardState) => {
      state.shouldDisplayCounter = true;
      state.statistics.achievedPoints = 0;
      state.statistics.lastMatchSucceeded = null;
      state.countryItems = shuffle(state.countryItems);
      state.personItems = shuffle(state.personItems);
      state.passedPersonIds = [];
      state.showingPersonId = null;
      state.isFinished = false;
    },
    start: (state: BoardState) => {
      state.shouldDisplayCounter = false;
    },
    peekNext: (state: BoardState) => {
      if (state.showingPersonId !== null)
        state.passedPersonIds.push(state.showingPersonId);

      if (state.personItems.length > state.passedPersonIds.length)
        state.showingPersonId = state.personItems[state.passedPersonIds.length].id;
      else {
        state.showingPersonId = null;
        state.isFinished = true;
      }
    },
    matched: {
      prepare: (payload: number, succeeded: boolean) => ({ payload, meta: { succeeded } }),
      reducer(
        state: BoardState,
        action: PayloadAction<number, string, { succeeded: boolean; }>
      ) {
        state.statistics.achievedPoints += action.meta.succeeded ? 20 : -5;
        state.statistics.lastMatchSucceeded = action.meta.succeeded;
      }
    }
  },
});

export const { generateBoard, start, peekNext, matched } = boardSlice.actions;

export default boardSlice.reducer;