import './BoardCounterText.css';

import React from 'react';

interface Props {
  text: string;
  delay: number;
  duration: number;
};

const BoardCounterText = ({ text, delay, duration }: Props) => (
  <text
    className="BoardCounterText"
    style={{ '--delay': `${delay}s`, '--duration': `${duration}s` }} x="0" y="50">
    {text}
  </text>
);

export default BoardCounterText;;
