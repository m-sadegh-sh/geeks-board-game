import './BoardCounter.css';

import React, { useEffect } from 'react';

import BoardCounterText from './BoardCounterText';
import { CounterItemInfo } from './types';

const counterItems: CounterItemInfo[] = [
  { text: '3' },
  { text: '2' },
  { text: '1' }
];

interface Props {
  onFinished: () => void;
};

const BoardCounter = ({ onFinished }: Props) => {
  const totalDuration = counterItems.reduce(
    (accumulator, _) => accumulator + 1, 0
  );

  useEffect(() => {
    const id = setTimeout(onFinished, totalDuration * 1000);
    return () => clearTimeout(id);
  }, [onFinished, totalDuration]);

  return (
    <div className="BoardCounter">
      <div className="BoardCounter-blurry"></div>
      <svg className="BoardCounter-surface" viewBox="-500 -500 1000 1000">
        <defs>
          <filter id="counterTextFilter">
            <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -5" result="goo"></feColorMatrix>
            <feComposite in="SourceGraphic" in2="goo" operator="atop"></feComposite>
          </filter>
        </defs>
        <circle className="BoardCounter-circle" cx="0" cy="0" r="200"></circle>
        <g filter="url(#counterTextFilter)">
          {counterItems.map((item, index) => {
            const delay = counterItems.filter(
              (_, _index) => _index < index
            ).reduce(
              (accumulator, _) => accumulator + 1, 0
            );

            return (
              <BoardCounterText
                key={item.text} text={item.text}
                delay={delay} duration={totalDuration} />
            );
          })}
        </g>
      </svg>
    </div>
  );
};

export default BoardCounter;
