import { BoardSurfaceCountryInfo, BoardSurfacePersonInfo } from './BoardSurface/types';

import { BoardStatisticsInfo } from './BoardStatistics/types';

export interface BoardState {
  shouldDisplayCounter: boolean;
  statistics: BoardStatisticsInfo;
  countryItems: BoardSurfaceCountryInfo[];
  personItems: BoardSurfacePersonInfo[];
  passedPersonIds: number[];
  showingPersonId: number | null;
  isFinished: boolean;
};
