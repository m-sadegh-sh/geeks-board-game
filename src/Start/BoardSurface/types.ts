export interface BoardSurfaceCountryInfo {
  isoCode: string;
  icon: string;
  name: string;
};

export interface BoardSurfacePersonInfo {
  id: number;
  countryIsoCode: string;
  picture: string;
  isPortrait: boolean;
};

export type MatchedCallback = (id: number, succeeded: boolean) => void;