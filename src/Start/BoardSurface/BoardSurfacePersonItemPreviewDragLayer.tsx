import './BoardSurfacePersonItemPreviewDragLayer.css';

import { BoardSurfacePersonInfo } from './types';
import BoardSurfacePersonItemPreview from './BoardSurfacePersonItemPreview';
import React from 'react';
import { useDragLayer } from 'react-dnd';

interface Props {
  personItems: BoardSurfacePersonInfo[];
}

const BoardSurfacePersonItemPreviewDragLayer = ({ personItems }: Props) => {
  const { isDragging, initialOffset, currentOffset, id } = useDragLayer((monitor) => ({
    isDragging: monitor.isDragging(),
    initialOffset: monitor.getInitialSourceClientOffset(),
    currentOffset: monitor.getSourceClientOffset(),
    id: monitor.getItem()?.id as number
  }));

  if (!isDragging || !initialOffset || !currentOffset || !id)
    return null;

  const item = personItems.find(
    i => i.id === id
  ) ?? null;

  if (item === null)
    return null;

  return (
    <div className="BoardSurfacePersonItemPreviewDragLayer">
      <div
        className="BoardSurfacePersonItemPreviewDragLayer-floating"
        style={{ '--x': `${currentOffset.x}px`, '--y': `${currentOffset.y}px` }}>
        <BoardSurfacePersonItemPreview className="" item={item} />
      </div>
    </div>
  );
};

export default BoardSurfacePersonItemPreviewDragLayer;
