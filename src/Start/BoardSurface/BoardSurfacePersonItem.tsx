import './BoardSurfacePersonItem.css';

import { BoardSurfacePersonInfo, MatchedCallback } from './types';
import { DragSourceMonitor, useDrag } from 'react-dnd';
import React, { useEffect } from 'react';

import { getEmptyImage } from 'react-dnd-html5-backend';

interface Props {
  className: string;
  num: number;
  item: BoardSurfacePersonInfo;
  onMatched: MatchedCallback;
};

const BoardSurfacePersonItem = ({ className, num, item, onMatched }: Props) => {
  const [{ isDragging }, drag, preview] = useDrag({
    item: { type: item.countryIsoCode, id: item.id },
    end: (item: { id: number; } | undefined, monitor: DragSourceMonitor) => {
      if (!item)
        return;

      onMatched(item.id, monitor.didDrop());
    },
    collect: (monitor: DragSourceMonitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true });
  }, [preview]);

  className = `BoardSurfacePersonItem${isDragging ? ' BoardSurfacePersonItem-dragging' : ''} ${item.isPortrait ? 'BoardSurfacePersonItem-portrait' : 'BoardSurfacePersonItem-landscape'} ${className}`;

  return (
    <div ref={drag} className={className}>
      <span className="BoardSurfacePersonItem-num">{num}</span>
      <img className="BoardSurfacePersonItem-picture" src={item.picture} alt={`Person #${item.id}`} />
    </div>
  );
};

export default BoardSurfacePersonItem;
