import './BoardSurface.css';

import { BoardSurfaceCountryInfo, BoardSurfacePersonInfo, MatchedCallback } from './types';

import BoardSurfaceCountryItem from './BoardSurfaceCountryItem';
import BoardSurfacePersonItem from './BoardSurfacePersonItem';
import BoardSurfacePersonItemPreviewDragLayer from './BoardSurfacePersonItemPreviewDragLayer';
import Container from '../../Container';
import React from 'react';

interface Props {
  countryItems: BoardSurfaceCountryInfo[];
  personItems: BoardSurfacePersonInfo[];
  showingPersonItem: BoardSurfacePersonInfo | null;
  onMatched: MatchedCallback;
}

const BoardSurface = ({ countryItems, personItems, showingPersonItem, onMatched }: Props) => {
  const showingPersonNum = personItems.findIndex(
    item => item.id === showingPersonItem?.id
  ) + 1;

  return (
    <Container className="BoardSurface">
      {countryItems.map((item, index) => (
        <BoardSurfaceCountryItem
          key={item.isoCode} className={`BoardSurfaceCountryItem-${index}`}
          item={item} />
      ))}
      <BoardSurfacePersonItemPreviewDragLayer personItems={personItems} />
      {showingPersonItem !== null &&
        <BoardSurfacePersonItem
          key={showingPersonItem.id} className="BoardSurfacePersonItem-centered"
          num={showingPersonNum} item={showingPersonItem} onMatched={onMatched} />
      }
    </Container>
  );
};

export default BoardSurface;