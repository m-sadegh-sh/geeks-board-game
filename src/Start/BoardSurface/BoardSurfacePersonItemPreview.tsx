import './BoardSurfacePersonItemPreview.css';

import { BoardSurfacePersonInfo } from './types';
import React from 'react';

interface Props {
  className: string;
  item: BoardSurfacePersonInfo;
};

const BoardSurfacePersonItemPreview = ({ className, item }: Props) => {
  className = `BoardSurfacePersonItemPreview ${item.isPortrait ? 'BoardSurfacePersonItemPreview-portrait' : 'BoardSurfacePersonItemPreview-landscape'} ${className}`;

  return (
    <div className={className}>
      <img className="BoardSurfacePersonItemPreview-picture" src={item.picture} alt={`Person #${item.id}`} />
    </div>
  );
};

export default BoardSurfacePersonItemPreview;
