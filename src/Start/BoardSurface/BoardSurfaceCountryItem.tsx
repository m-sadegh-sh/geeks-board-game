import './BoardSurfaceCountryItem.css';

import { DropTargetMonitor, useDrop } from 'react-dnd';

import { BoardSurfaceCountryInfo } from './types';
import React from 'react';

interface Props {
  className: string;
  item: BoardSurfaceCountryInfo;
};

const BoardSurfaceCountryItem = ({ className, item }: Props) => {
  const [{ isDropping }, drop] = useDrop({
    accept: item.isoCode,
    collect: (monitor: DropTargetMonitor) => ({
      isDropping: monitor.isOver() && monitor.canDrop(),
    }),
  });

  className = `BoardSurfaceCountryItem${isDropping ? ' BoardSurfaceCountryItem-dropping' : ''} ${className}`;

  return (
    <div ref={drop} className={className}>
      <img src={item.icon} alt={item.name} />
      <label className="BoardSurfaceCountryItem-name">{item.name}</label>
    </div>
  );
};

export default BoardSurfaceCountryItem;
