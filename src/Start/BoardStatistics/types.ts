export interface BoardStatisticsInfo {
    achievedPoints: number;
    lastMatchSucceeded: boolean | null;
};