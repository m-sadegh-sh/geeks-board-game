import './BoardStatistics.css';

import BoardStatisticItem from './BoardStatisticItem';
import { BoardStatisticsInfo } from './types';
import Container from '../../Container';
import React from 'react';

interface Props {
  statistics: BoardStatisticsInfo;
};

const BoardStatistics = ({ statistics }: Props) => (
  <Container className={`BoardStatistics ${statistics.achievedPoints < 0 ? 'BoardStatistics-negative' : statistics.achievedPoints > 0 ? 'BoardStatistics-positive' : 'BoardStatistics-zero'}`}>
    <BoardStatisticItem
      className={`BoardStatisticItem-achieved-points${statistics.lastMatchSucceeded === true ? 'BoardStatisticItem-succeeded' : statistics.lastMatchSucceeded === false ? 'BoardStatisticItem-failed' : ''}`}
      label="My Points" value={statistics.achievedPoints} />
  </Container>
);

export default BoardStatistics;
