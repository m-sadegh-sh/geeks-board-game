import './BoardStatisticItem.css';

import React, { useEffect, useRef, useState } from 'react';

interface Props {
  className: string;
  label: string;
  value: number;
};

const BoardStatisticItem = ({ className, label, value }: Props) => {
  const [updating, setUpdating] = useState(false);
  const isInitialMount = useRef(true);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      setUpdating(true);

      const id = setTimeout(() => setUpdating(false), 1000);

      return () => clearTimeout(id);
    }
  }, [value]);

  return (
    <div className={`BoardStatisticItem${updating ? ' BoardStatisticItem-updating' : ''} ${className}`}>
      <label className="BoardStatisticItem-label">{label}</label>
      <span className="BoardStatisticItem-value">{value}</span>
    </div>
  );
};

export default BoardStatisticItem;
