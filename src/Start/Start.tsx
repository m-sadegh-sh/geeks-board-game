import './Start.css';

import React, { useCallback, useEffect } from 'react';
import { countryItemsSelector, isFinishedSelector, personItemsSelector, shouldDisplayCounterSelector, showingPersonItemSelector, statisticsSelector } from './selectors';
import { generateBoard, peekNext, start } from './boardSlice';
import { useDispatch, useSelector } from 'react-redux';

import AlertDialog from '../AlertDialog';
import BoardCounter from './BoardCounter';
import BoardStatistics from './BoardStatistics';
import BoardSurface from './BoardSurface';
import { CSSTransition } from 'react-transition-group';
import Container from '../Container';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { matchedAndPeekNextAsync } from './actionHandlers';

const Start = () => {
  const shouldDisplayCounter = useSelector(shouldDisplayCounterSelector);
  const statistics = useSelector(statisticsSelector);
  const countryItems = useSelector(countryItemsSelector);
  const personItems = useSelector(personItemsSelector);
  const showingPersonItem = useSelector(showingPersonItemSelector);
  const isFinished = useSelector(isFinishedSelector);

  const dispatch = useDispatch();

  const generateBoardCallback = useCallback(() => {
    dispatch(generateBoard());
  }, [dispatch]);

  const startCallback = useCallback(() => {
    dispatch(start());
  }, [dispatch]);

  const peekNextCallback = useCallback(() => {
    dispatch(peekNext());
  }, [dispatch]);

  const onMatchedCallback = useCallback((id: number, succeeded: boolean) => {
    dispatch(matchedAndPeekNextAsync(id, succeeded));
  }, [dispatch]);

  const restartCallback = useCallback(() => {
    dispatch(generateBoard());
    dispatch(peekNext());
    dispatch(start());
  }, [dispatch]);

  useEffect(generateBoardCallback, []);

  useEffect(() => {
    const id = setTimeout(peekNextCallback, 3000);
    return () => clearTimeout(id);
  }, [peekNextCallback, showingPersonItem]);

  return (
    <Container className="Start">
      <CSSTransition in={shouldDisplayCounter} timeout={300} classNames="BoardCounter" unmountOnExit>
        <BoardCounter onFinished={startCallback} />
      </CSSTransition>
      {isFinished && statistics.achievedPoints === 0 &&
        <AlertDialog
          className="Start-finished-alert Start-finished-alert-sleepy"
          title="Are you awake?"
          message={`Your round just finished without getting any points. Would you like to try again?`}
          action={<button onClick={restartCallback}>Restart</button>} />
      }
      {isFinished && statistics.achievedPoints < 0 &&
        <AlertDialog
          className="Start-finished-alert Start-finished-alert-slow-mind-guy"
          title="Opps! Too hard?"
          message={`You've lost this round with ${statistics.achievedPoints} points, however we've counted on you mate! We're waiting for the compensation of this result. Would you like to try again?`}
          action={<button onClick={restartCallback}>Restart</button>} />
      }
      {isFinished && statistics.achievedPoints > 0 &&
        <AlertDialog
          className="Start-finished-alert Start-finished-alert-smart-guy"
          title="Hurray! You've beat Geeks!"
          message={`You've got +${statistics.achievedPoints} points in this round, although we hope you could break your score. Would you like to try again?`}
          action={<button onClick={restartCallback}>Restart</button>} />
      }
      <DndProvider backend={HTML5Backend}>
        <BoardSurface
          countryItems={countryItems} personItems={personItems}
          showingPersonItem={showingPersonItem} onMatched={onMatchedCallback} />
        <BoardStatistics statistics={statistics} />
      </DndProvider>
    </Container>
  );
};

export default Start;
