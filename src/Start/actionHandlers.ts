import { matched, peekNext } from "./boardSlice";

import { boardSelector } from "./selectors";

export const matchedAndPeekNextAsync = (id: number, succeeded: boolean) => (dispatch: any, getState: any) => {
    const rootState = getState();
    const boardState = boardSelector(rootState);

    if (boardState.showingPersonId !== id)
        return;

    if (boardState.passedPersonIds.includes(id))
        return;

    dispatch(matched(id, succeeded));
    dispatch(peekNext());
};
