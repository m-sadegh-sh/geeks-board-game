import { RootState } from "../types";
import { createSelector } from "@reduxjs/toolkit";

export const boardSelector = (state: RootState) => state.board;

export const shouldDisplayCounterSelector = createSelector(
    boardSelector,
    state => state.shouldDisplayCounter
);
export const statisticsSelector = createSelector(
    boardSelector,
    state => state.statistics
);
export const countryItemsSelector = createSelector(
    boardSelector,
    state => state.countryItems
);
export const personItemsSelector = createSelector(
    boardSelector,
    state => state.personItems
);
export const passedPersonIdsSelector = createSelector(
    boardSelector,
    state => state.passedPersonIds
);
export const showingPersonIdSelector = createSelector(
    boardSelector,
    state => state.showingPersonId
);
export const showingPersonItemSelector = createSelector(
    [personItemsSelector, showingPersonIdSelector],
    (personItems, showingPersonId) => {
        if (showingPersonId === null)
            return null;

        return personItems.find(item => item.id === showingPersonId) ?? null;
    }
);
export const isFinishedSelector = createSelector(
    boardSelector,
    state => state.isFinished
);
