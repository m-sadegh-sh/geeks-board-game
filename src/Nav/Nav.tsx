import './Nav.css';

import NavItem from './NavItem';
import { NavItemInfo } from './types';
import React from 'react';
import { faCog } from '@fortawesome/free-solid-svg-icons';

const primaryItems: NavItemInfo[] = [
  { url: `${process.env.PUBLIC_URL}/`, text: 'How to Play' },
  { url: `${process.env.PUBLIC_URL}/start`, text: 'Start Game' }
];

const secondaryItems: NavItemInfo[] = [
  { url: `${process.env.PUBLIC_URL}/settings`, text: 'Settings', icon: faCog }
];

const Nav = () => (
  <nav className="Nav">
    <div className="Nav-primary">
      {primaryItems.map(item => <NavItem key={item.url} item={item} />)}
    </div>
    <div className="Nav-secondary">
      {secondaryItems.map(item => <NavItem key={item.url} item={item} />)}
    </div>
  </nav>
);

export default Nav;
