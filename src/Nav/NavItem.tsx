import './NavItem.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavItemInfo } from './types';
import { NavLink } from "react-router-dom";
import React from 'react';

interface Pros {
  item: NavItemInfo;
};

const NavItem = ({ item }: Pros) => (
  <NavLink key={item.url} className="Nav-link" activeClassName="Nav-link-active" to={item.url} exact>
    {item.icon && <FontAwesomeIcon className="Nav-link-icon" icon={item.icon} />}
    {item.text && <span className="Nav-link-text">{item.text}</span>}
  </NavLink>
);

export default NavItem;
