import { IconDefinition } from "@fortawesome/fontawesome-svg-core";

export interface NavItemInfo {
    url: string;
    text?: string;
    icon?: IconDefinition;
};
