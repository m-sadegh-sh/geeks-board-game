declare module 'csstype' {
    interface Properties {
        '--duration'?: number;
        '--delay'?: number;
        '--x'?: number;
        '--y'?: number;
    }
}