import { BoardState } from './Start/types';

export interface RootState {
  board: BoardState;
};
