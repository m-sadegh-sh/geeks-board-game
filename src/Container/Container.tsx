import './Container.css';

import React from 'react';

interface Props {
  fillHeight?: boolean;
  className?: string;
  children: any;
};

const Container = ({ fillHeight, className, children, ...others }: Props) => {
  others = {
    ...others,
    className: `Container${fillHeight ? ' Container-fill-height' : ''}${className ? ` ${className}` : ''}`
  };

  return (
    <div {...others}>
      {children}
    </div>
  );
};

export default Container;