import './NotFound.css';

import Alert from '../Alert';
import Container from '../Container';
import { Link } from 'react-router-dom';
import React from 'react';

const NotFound = () => (
  <Container className="NotFound">
    <Alert
      className="NotFound-alert"
      title="404 :("
      message="Oops! We couldn't find what you are looking for. Please let us take you back to the starting point."
      action={<Link to={`${process.env.PUBLIC_URL}/`}>Back</Link>} />
  </Container>
);

export default NotFound;
