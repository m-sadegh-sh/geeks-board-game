import { RootState } from './types';
import boardReducer from './Start/boardSlice';
import { configureStore } from '@reduxjs/toolkit';

const store = configureStore<RootState>({
  reducer: {
    board: boardReducer,
  },
  devTools: process.env.NODE_ENV !== 'production'
});

export default store;