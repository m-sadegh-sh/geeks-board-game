import './App.css';

import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

import Container from './Container';
import Nav from './Nav';
import NotFound from './NotFound';
import React from 'react';
import Start from './Start';
import Welcome from './Welcome';

const App = () => (
  <Container fillHeight className="App">
    <Router>
      <Nav />
      <Switch>
        <Route path={`${process.env.PUBLIC_URL}/`} exact>
          <Welcome />
        </Route>
        <Route path={`${process.env.PUBLIC_URL}/start`} exact>
          <Start />
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </Router>
  </Container>
);

export default App;
