import './AlertDialog.css';

import Alert from '../Alert';
import React from 'react';

interface Props {
  className?: string;
  logo?: string;
  title: string;
  message: string;
  action?: any;
};

const AlertDialog = (props: Props) => {
  return (
    <div className="AlertDialog">
      <Alert {...props} />
    </div>
  );
};

export default AlertDialog;