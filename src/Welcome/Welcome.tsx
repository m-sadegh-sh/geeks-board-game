import './Welcome.css';

import Alert from '../Alert';
import Container from '../Container';
import { Link } from 'react-router-dom';
import React from 'react';
import logo from '../assets/images/logo.svg';

const Welcome = () => (
  <Container className="Welcome">
    <Alert
      className="Welcome-alert"
      logo={logo}
      title="Geeks Board Game"
      message="Welcome to Geeks Board Game, a React-based app developed by Mohammad for Geeks' guys."
      action={<Link to={`${process.env.PUBLIC_URL}/start`}>Let's Start</Link>} />
  </Container>
);

export default Welcome;
