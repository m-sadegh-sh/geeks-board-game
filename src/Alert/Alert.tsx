import './Alert.css';

import Container from '../Container';
import React from 'react';

interface Props {
  className?: string;
  logo?: string;
  title: string;
  message: string;
  action?: any;
};

const Alert = ({ className, logo, title, message, action }: Props) => {
  className = `Alert${className ? ` ${className}` : ''}`;

  return (
    <div className={className}>
      {logo &&
        <div className="Alert-logo">
          <img src={logo} className="Alert-logo-img" alt={title} />
        </div>
      }
      <Container className="Alert-content">
        <h2 className="Alert-content-title">{title}</h2>
        <p className="Alert-content-message">{message}</p>
        {action &&
          <div className="Alert-content-actions">{action}</div>
        }
      </Container>
    </div>
  );
};

export default Alert;